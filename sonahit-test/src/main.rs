fn test(s: &str) -> Box<str> {
    String::from(s).into_boxed_str()
}

macro_rules! help {
    ($($x:expr), *) => {
        {
            let mut temp_vec = Vec::new();
            $(
                temp_vec.push($x);
            )*
            temp_vec
        }
    };
}
fn main() {
    let asd = help!(1, 2, 3, 4);

    println!("{:#?}", asd);
}
