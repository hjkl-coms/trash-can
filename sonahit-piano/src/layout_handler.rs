// ██████████████
// ██████████████
// ██████████████
// CCDDEEFFGGAABB
//  C#D#  F#G#A#
//  DbEb  GBAbBb

// PRESS C

// ░░████████████
// ░░████████████
// ░░████████████
// CCDDEEFFGGAABB
//  C#D#  F#G#A#
//  DbEb  GBAbBb

// PRESS Gb

// ███████░░█████
// ███████░░█████
// ██████████████
// CCDDEEFFGGAABB
//  C#D#  F#G#A#
//  DbEb  GBAbBb

// C - A
// D - S
// E - D
// F - F
// G - G
// A - H
// B - J
// C#
// D#
// F#
// G#
// A#
// Db
// Eb
// GB
// Ab
// Bb
