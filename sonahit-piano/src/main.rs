extern crate regex;
extern crate rodio;
extern crate synthrs;
extern crate tokio;

use rodio::Source;
use std::path::Path;
use std::time::Duration;

mod layout_handler;
mod sound;
mod synth;

#[tokio::main]
async fn main() {
    let root_dir = std::env::current_dir().unwrap();
    let root_dir_str = root_dir.to_str().unwrap();
    let sond_dir = sound::get_sounds_folder(root_dir_str);
    let song_folder = sond_dir.clone();
    let notes = sound::get_notes();
    let note = notes.first().unwrap();
    synth::generate::create_note_sample(5.0, note, Path::new(&song_folder))
    // notes.iter().clone().for_each(|note| {
    //     synth::generate::create_note_sample_if_not_exists(5.0, note, Path::new(&song_folder))
    // });
    // let devices = sound::get_devices();
    // {
    //     {
    //         let keys = sound::get_key_sounds()
    //             .into_iter()
    //             .map(|(v, _)| v)
    //             .collect::<Vec<sound::Keys>>();
    //         for key in keys {
    //             let key_path = sound::get_key_path(&key, Path::new(&song_folder));
    //             for device in devices.iter() {
    //                 let result = sound::get_sound_handler(key_path.clone(), &device);
    //                 if let Some((source, handler)) = result {
    //                     let join_handle: JoinHandle<_> =
    //                         tokio::task::spawn(play_sound(source, handler));
    //                     join_handle.await.unwrap_or(());
    //                 }
    //             }
    //         }
    //     }
    // }
}

async fn play_sound(
    source: rodio::Decoder<std::io::BufReader<std::fs::File>>,
    handler: rodio::OutputStreamHandle,
) {
    let r = handler.play_raw(source.convert_samples());
    if let Ok(_) = r {
        std::thread::sleep(Duration::from_secs(3));
    }
}
