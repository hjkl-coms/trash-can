use crate::sound::{get_note_sound, Keys, KeysModif, Note, A4_KEY_FREQ};
use std::fs::File;
use std::path::Path;
use synthrs::music::note;
use synthrs::synthesizer::{make_samples, quantize_samples};
use synthrs::wave::sine_wave;
use synthrs::writer::write_wav_file;

fn get_semitome(create_note: &Note) -> Option<u8> {
    let Note {
        octave: _,
        key,
        modif,
    } = create_note;

    let semitome = match *key {
        Keys::A => 9,
        Keys::B => 10,
        Keys::C => match modif {
            KeysModif::Sharp => 1,
            _ => 0,
        },
        Keys::D => match modif {
            KeysModif::Sharp => 3,
            _ => 2,
        },
        Keys::E => 4,
        Keys::F => match modif {
            KeysModif::Sharp => 6,
            _ => 5,
        },
        Keys::G => match modif {
            KeysModif::Sharp => 8,
            _ => 7,
        },
    };
    let note_str = get_note_sound(create_note);
    let note_str = note_str.clone();
    let alias = crate::sound::find_alias(note_str.as_str());
    if let Some(alias) = alias {
        let note = crate::sound::create_from_str(alias);
        if let Ok(n) = note {
            get_semitome(&n)
        } else {
            None
        }
    } else {
        Some(semitome)
    }
}

pub fn create_note_sample_if_not_exists(time: f64, create_note: &Note, folder: &Path) {
    let note_str = get_note_sound(create_note);
    let note_path = folder.join(note_str.clone());
    let file = File::open(note_path);
    if let Err(_) = file {
        create_note_sample(time, create_note, folder)
    }
}

pub fn create_note_sample(time: f64, create_note: &Note, folder: &Path) {
    let Note {
        octave,
        key: _,
        modif: _,
    } = create_note;

    let note_str = get_note_sound(create_note);
    let note_path = folder.join(note_str.clone());
    let result = get_semitome(create_note);
    if let Some(semitome) = result {
        let note_freq = note(A4_KEY_FREQ, semitome as usize, *octave as usize);
        let samples = make_samples(1.0, 44_100, sine_wave(note_freq));
        write_wav_file(
            (note_path.to_str().unwrap().to_owned() + ".wav").as_str(),
            44_100,
            &quantize_samples::<i16>(&samples),
        )
        .unwrap();
    }
}
