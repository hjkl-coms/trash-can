use cpal::traits::{DeviceTrait, HostTrait};
use std::collections::HashMap;
use std::fs::File;
use std::hash::Hash;
use std::io::BufReader;
use std::path::{Path, PathBuf};

const ASSETS_FOLDER: &str = "assets";
const MAX_OCTAVE: u8 = 8;
const KEYS_VARIANTS: u8 = 7;
const KEYS_MODIF_VARIANTS: u8 = 3;
pub const A4_KEY_FREQ: f64 = 440.0;

#[derive(Debug, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum Keys {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
}

#[derive(Debug, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum KeysModif {
    Be,
    Sharp,
    None,
}

#[derive(Debug, Hash)]
pub struct Note {
    pub key: Keys,
    pub modif: KeysModif,
    pub octave: u8,
}

#[macro_export]
macro_rules! hash_map {
    ($(($key: expr, $value: expr)), *) => {
        {
            let mut map = HashMap::new();
            $(
                map.insert($key, $value);
            )*
            map
        }
    };
}

pub fn get_sounds_folder(root_folder: &str) -> String {
    let root_path = Path::new(root_folder).join(ASSETS_FOLDER).join("sounds");
    String::from(root_path.to_str().unwrap())
}

pub fn find_alias(key: &str) -> Option<&str> {
    let aliases = get_aliases();
    let found_alias = aliases.iter().clone().find(|(v, _)| **v == key);
    match found_alias {
        Some((v, _)) => Some(*v),
        None => None,
    }
}

pub fn get_aliases() -> HashMap<&'static str, &'static str> {
    hash_map!(
        ("C#0", "Db0"),
        ("D#0", "Eb0"),
        ("F#0", "Gb0"),
        ("G#0", "Ab0"),
        ("A#0", "Bb0"),
        ("C#1", "Db1"),
        ("D#1", "Eb1"),
        ("F#1", "Gb1"),
        ("G#1", "Ab1"),
        ("A#1", "Bb1"),
        ("C#2", "Db2"),
        ("D#2", "Eb2"),
        ("F#2", "Gb2"),
        ("G#2", "Ab2"),
        ("A#2", "Bb2"),
        ("C#3", "Db3"),
        ("D#3", "Eb3"),
        ("F#3", "Gb3"),
        ("G#3", "Ab3"),
        ("A#3", "Bb3"),
        ("C#4", "Db4"),
        ("D#4", "Eb4"),
        ("F#4", "Gb4"),
        ("G#4", "Ab4"),
        ("A#4", "Bb4"),
        ("C#5", "Db5"),
        ("D#5", "Eb5"),
        ("F#5", "Gb5"),
        ("G#5", "Ab5"),
        ("A#5", "Bb5"),
        ("C#6", "Db6"),
        ("D#6", "Eb6"),
        ("F#6", "Gb6"),
        ("G#6", "Ab6"),
        ("A#6", "Bb6"),
        ("C#7", "Db7"),
        ("D#7", "Eb7"),
        ("F#7", "Gb7"),
        ("G#7", "Ab7"),
        ("A#7", "Bb7"),
        ("C#8", "Db8"),
        ("D#8", "Eb8"),
        ("F#8", "Gb8"),
        ("G#8", "Ab8"),
        ("A#8", "Bb8")
    )
}

fn get_key_str(key: &Keys) -> &str {
    match key {
        Keys::A => "A",
        Keys::B => "B",
        Keys::C => "C",
        Keys::D => "D",
        Keys::E => "E",
        Keys::F => "F",
        Keys::G => "G",
    }
}

fn get_key_from_str(key: &str) -> Option<Keys> {
    match key {
        "A" => Some(Keys::A),
        "B" => Some(Keys::B),
        "C" => Some(Keys::C),
        "D" => Some(Keys::D),
        "E" => Some(Keys::E),
        "F" => Some(Keys::F),
        "G" => Some(Keys::G),
        _ => None,
    }
}

fn get_key_modif_from_str(modif: &str) -> Option<KeysModif> {
    match modif {
        "b" => Some(KeysModif::Be),
        "#" => Some(KeysModif::Sharp),
        _ => None,
    }
}

fn get_key_modif_str(modif: &KeysModif) -> &str {
    match modif {
        KeysModif::Be => "b",
        KeysModif::Sharp => "#",
        _ => "",
    }
}

pub fn get_note_sound(note: &Note) -> String {
    let Note { key, modif, octave } = note;
    let note = {
        let key_modif = get_key_modif_str(modif);
        let key_str = get_key_str(key);
        String::from(key_str).to_owned() + key_modif
    };
    let note_str = note.to_owned() + octave.to_string().as_str();
    String::from(note_str.as_str())
}

pub fn create_from_str(key: &str) -> Result<Note, String> {
    let pattern =
        regex::Regex::new(r"(?P<note>[ABCDEFGabcdefg])(?P<mod>#|b)?(<octave>\d+)").unwrap();
    if !pattern.is_match(key) {
        Err(String::from("wrong note"))
    } else {
        let captures = pattern.captures(key).unwrap();
        let note = captures.get(0).unwrap().as_str();
        let modif = captures.get(1).map_or("", |v| v.as_str());
        let octave = captures
            .get(2)
            .map_or(0, |m| m.as_str().parse::<u8>().unwrap());

        let note = get_key_from_str(note).unwrap();
        let modif = get_key_modif_from_str(modif).unwrap();
        Ok(Note {
            key: note,
            modif,
            octave,
        })
    }
}

pub fn get_notes() -> Vec<Note> {
    let mut notes: Vec<Note> = Vec::new();
    let notes_len = (MAX_OCTAVE * KEYS_VARIANTS * KEYS_MODIF_VARIANTS) as usize;
    notes.reserve(notes_len);
    for i in 1..=notes_len {
        let octave = (i % (MAX_OCTAVE as usize)) as u8;
        let key = match i % (KEYS_VARIANTS as usize) {
            1 => Keys::A,
            2 => Keys::B,
            3 => Keys::C,
            4 => Keys::D,
            5 => Keys::E,
            6 => Keys::F,
            7 => Keys::G,
            _ => Keys::A,
        };
        let modif = match i % (KEYS_MODIF_VARIANTS as usize) {
            1 => KeysModif::Be,
            2 => KeysModif::Sharp,
            _ => KeysModif::None,
        };
        let note = Note { key, octave, modif };
        notes.push(note);
    }
    notes
}

pub fn get_note_path(note: &Note, song_folder: &Path) -> String {
    let key = get_note_sound(note);
    let buffer = song_folder.to_path_buf();
    let song_path = PathBuf::from(buffer);

    String::from(song_path.join(key.to_owned() + ".mp3").to_str().unwrap())
}

pub fn get_devices() -> Vec<cpal::Device> {
    let host = cpal::default_host();
    let devices = host.devices().unwrap().collect::<Vec<cpal::Device>>();
    devices
}

pub fn get_sound_handler<'a>(
    sound_key: String,
    device: &'a cpal::Device,
) -> Option<(
    rodio::Decoder<std::io::BufReader<std::fs::File>>,
    rodio::OutputStreamHandle,
)> {
    let file = File::open(sound_key);
    if let Ok(file_descriptor) = file {
        let mut supported_configs_range = device
            .supported_output_configs()
            .expect("error while querying configs");
        let supported_config = supported_configs_range.next();
        if let Some(_) = supported_config {
            let result = rodio::OutputStream::try_from_device(device);
            match result {
                Ok(r) => {
                    let (_stream, stream_handle) = r;
                    let source = rodio::Decoder::new(BufReader::new(file_descriptor)).unwrap();
                    Some((source, stream_handle))
                }
                Err(r) => {
                    println!("{}", r);
                    None
                }
            }
        } else {
            None
        }
    } else {
        None
    }
}
