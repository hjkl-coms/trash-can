use crate::types::entity::Entity;

pub trait Damage {
    fn attack(&mut self, defender: &mut Entity) -> ();
    fn get_attack(&self) -> i32;
}

pub trait Defence {
    fn defence(&mut self, attacker: &Entity) -> ();
    fn get_defence(&self) -> i32;
}

pub trait Health {
    fn add_health(&mut self, points: i32) -> i32;
    fn drop_health(&mut self, points: i32) -> i32;
    fn is_dead(&self) -> bool;
}

pub trait Action: Health + Damage + Defence {
    fn action(&mut self, other: &mut Entity, action: EntityActions);
}

#[derive(Copy, Clone, PartialEq)]
pub enum EntityActions {
    Attack,
    Defence,
    Nothing,
    Flee,
}
