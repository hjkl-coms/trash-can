#[derive(Copy, Clone, PartialEq)]
pub enum GameViews {
    GameHelloMenuView,
    GameMenuViews,
}

#[derive(Copy, Clone, PartialEq)]
pub enum GameStartActions {
    Start,
    End,
}

#[derive(Copy, Clone, PartialEq)]
pub enum GameMenuActions {
    Start,
    Exit,
}
