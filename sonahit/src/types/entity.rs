pub const DEFAULT_HEALTH: i32 = 100;
pub const DEFAULT_DAMAGE: i32 = 10;
pub const DEFAULT_DEFENCE: i32 = 0;

pub const DEFAULT_MOB_HEALTH: i32 = 20;
pub const DEFAULT_MOB_DAMAGE: i32 = 1;
pub const DEFAULT_MOB_DEFENCE: i32 = 1;

#[derive(Copy, Clone)]
pub struct Entity {
    pub hp: i32,
    pub damage: i32,
    pub defence: i32,
}

#[derive(Copy, Clone)]
pub struct Hero {
    pub entity: Entity,
}

#[derive(Copy, Clone)]
pub struct Mob {
    pub entity: Entity,
}
