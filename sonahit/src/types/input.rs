#[allow(dead_code)]
pub struct Input {
    last_input: String,
    current_input: String,
}

pub type StdOutTermion = termion::raw::RawTerminal<std::io::Stdout>;
