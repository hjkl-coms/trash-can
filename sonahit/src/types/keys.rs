pub const KEY_SELECT: char = '←';
pub const KEY_EMPTY: char = ' ';

pub const KEY_ENTER: char = '\n';
pub const KEY_QUIT: char = 'q';

pub const KEY_UP: char = 'u';
pub const KEY_DOWN: char = 'd';
