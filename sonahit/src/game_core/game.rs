use crate::game_core::views;
use crate::types::entity::*;
use crate::types::entity_actions::{Action, EntityActions, Health};
use crate::types::game_actions::*;
use crate::types::input::StdOutTermion;
use crate::types::keys;

use std::io::Write;

use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

pub struct Game {
    pub hero: Hero,
    pub mobs: Vec<Mob>,
    pub view: GameViews,
}

fn change_cursor(cursor: usize, max: usize, up: bool) -> usize {
    match up {
        true => {
            if cursor >= max {
                0
            } else {
                cursor + 1
            }
        }
        false => {
            if cursor <= 0 {
                max
            } else {
                cursor - 1
            }
        }
    }
}

impl Game {
    pub fn new(hero: Hero) -> Self {
        Game {
            hero: hero,
            mobs: Vec::new(),
            view: GameViews::GameHelloMenuView,
        }
    }

    fn init(&self) -> () {
        match self.try_start_game() {
            GameStartActions::Start => self.start_game(),
            GameStartActions::End => self.end_game(),
        }
        self.end_game();
    }

    fn clear_cli(&self, stdout: &mut StdOutTermion) {
        write!(
            stdout,
            "{}{}{}",
            termion::clear::All,
            termion::cursor::Show,
            termion::cursor::Goto(1, 1)
        )
        .unwrap();
    }

    fn end_game(&self) {
        std::process::exit(0);
    }

    fn try_start_game(&self) -> GameStartActions {
        let mut game_started = GameStartActions::End;
        {
            let stdin = std::io::stdin();
            let mut stdout = Box::new(std::io::stdout().into_raw_mode().unwrap());

            views::hello::view(&mut stdout);
            stdout.flush().unwrap();

            for c in stdin.keys() {
                views::hello::view(&mut stdout);
                stdout.flush().unwrap();
                let action = match c.unwrap() {
                    Key::Char(keys::KEY_QUIT) => Some(GameStartActions::End),
                    Key::Char(keys::KEY_ENTER) => Some(GameStartActions::Start),
                    _ => None,
                };
                if action.is_some() {
                    game_started = action.unwrap();
                    break;
                }
            }

            self.clear_cli(&mut stdout);
        }

        game_started
    }

    fn start_game(&self) -> () {
        let game_actions = [GameMenuActions::Start, GameMenuActions::Exit];
        let get_menu_action = |cursor: usize| game_actions[cursor];
        let stdin = std::io::stdin();
        let mut stdout = Box::new(std::io::stdout().into_raw_mode().unwrap());
        let mut cursor = 0;
        self.clear_cli(&mut stdout);
        stdout.flush().unwrap();
        views::menu::view(&mut stdout, get_menu_action(cursor));

        for c in stdin.keys() {
            let key = match c.unwrap() {
                Key::Down => Some(keys::KEY_DOWN),
                Key::Up => Some(keys::KEY_UP),
                Key::Char(keys::KEY_ENTER) => Some(keys::KEY_ENTER),
                Key::Ctrl('d') => Some(keys::KEY_QUIT),
                Key::Ctrl('c') => Some(keys::KEY_QUIT),
                _ => None,
            };
            match key {
                Some(keys::KEY_DOWN) => {
                    cursor = change_cursor(cursor, game_actions.len() - 1, true);
                }
                Some(keys::KEY_UP) => {
                    cursor = change_cursor(cursor, game_actions.len() - 1, false);
                }
                Some(keys::KEY_ENTER) => {
                    self.clear_cli(&mut stdout);
                    match get_menu_action(cursor) {
                        GameMenuActions::Start => self.play(&mut stdout),
                        GameMenuActions::Exit => {
                            stdout.flush().unwrap();
                            break;
                        }
                    }
                }
                Some(keys::KEY_QUIT) => {
                    break;
                }
                _ => {
                    stdout.flush().unwrap();
                }
            }
            views::menu::view(&mut stdout, get_menu_action(cursor));
        }
    }

    fn view_game_over(&self, stdout: &mut StdOutTermion) {
        let stdin = std::io::stdin();

        self.clear_cli(stdout);
        views::game_over::view(stdout);
        stdout.flush().unwrap();
        for game_over_c in stdin.keys() {
            views::game_over::view(stdout);
            stdout.flush().unwrap();
            let key = match game_over_c.unwrap() {
                Key::Char(keys::KEY_ENTER) => Some(keys::KEY_QUIT),
                Key::Char(keys::KEY_QUIT) => Some(keys::KEY_QUIT),
                Key::Ctrl('d') => Some(keys::KEY_QUIT),
                Key::Ctrl('c') => Some(keys::KEY_QUIT),
                _ => None,
            };
            if key.unwrap_or('\n') == keys::KEY_QUIT {
                break;
            }
            self.clear_cli(stdout);
        }
        stdout.flush().unwrap();
    }

    fn play(&self, stdout: &mut StdOutTermion) {
        let game_actions = [
            EntityActions::Attack,
            EntityActions::Defence,
            EntityActions::Nothing,
            EntityActions::Flee,
        ];
        let get_menu_action = |cursor: usize| game_actions[cursor];
        let mut defeated_mobs = 0;
        let mut mob = Mob::new(DEFAULT_MOB_HEALTH, DEFAULT_MOB_DAMAGE, DEFAULT_MOB_DEFENCE);
        let mut hero = self.hero;
        let mut cursor = 0;
        let stdin = std::io::stdin();

        views::play::view(stdout, &hero, &mob, get_menu_action(cursor), defeated_mobs);
        for c in stdin.keys() {
            views::play::view(stdout, &hero, &mob, get_menu_action(cursor), defeated_mobs);
            let key = match c.unwrap() {
                Key::Down => Some(keys::KEY_DOWN),
                Key::Up => Some(keys::KEY_UP),
                Key::Char(keys::KEY_ENTER) => Some(keys::KEY_ENTER),
                Key::Char(keys::KEY_QUIT) => Some(keys::KEY_QUIT),
                Key::Ctrl('d') => Some(keys::KEY_QUIT),
                Key::Ctrl('c') => Some(keys::KEY_QUIT),
                _ => None,
            };
            match key {
                Some(keys::KEY_DOWN) => {
                    cursor = change_cursor(cursor, game_actions.len() - 1, true);
                }
                Some(keys::KEY_UP) => {
                    cursor = change_cursor(cursor, game_actions.len() - 1, false);
                }
                Some(keys::KEY_ENTER) => {
                    match get_menu_action(cursor) {
                        EntityActions::Flee => break,
                        EntityActions::Attack => {
                            hero.entity.action(&mut mob.entity, get_menu_action(cursor))
                        }
                        _ => (),
                    };
                    match mob.entity.is_dead() {
                        false => mob.entity.action(&mut hero.entity, EntityActions::Attack),
                        true => {
                            defeated_mobs += 1;
                            mob.entity.hp =
                                DEFAULT_MOB_HEALTH + (DEFAULT_MOB_HEALTH / 15 * defeated_mobs);
                            mob.entity.damage = DEFAULT_MOB_DAMAGE + defeated_mobs;
                            mob.entity.defence = defeated_mobs;
                        }
                    }
                }
                Some(keys::KEY_QUIT) => {
                    break;
                }
                _ => {
                    stdout.flush().unwrap();
                }
            }
            if hero.entity.is_dead() {
                self.view_game_over(stdout);
                break;
            } else {
                self.clear_cli(stdout);
                views::play::view(stdout, &hero, &mob, get_menu_action(cursor), defeated_mobs);
            }
        }
    }
}

pub fn init_game() {
    let hero = Hero::new(100, 10, 0);

    let game = Game::new(hero);
    game.init();
}
