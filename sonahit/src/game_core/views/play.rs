use crate::termion;
use crate::types::entity::*;
use crate::types::entity_actions::EntityActions;
use crate::types::keys::KEY_SELECT;
use std::io::Write;
use std::ops::Add;
use std::string::ToString;

use crate::types::input::StdOutTermion;

pub fn view(
    stdout: &mut StdOutTermion,
    hero: &Hero,
    mob: &Mob,
    select_action: EntityActions,
    defeated_mobs: i32,
) {
    let hero_entity = &(*hero).entity;
    let mob_entity = &(*mob).entity;

    write!(
        stdout,
        "
        {}{}#HERO{}#MOB{}\
        HP  {}{}HP  {}{}\
        ATT {}{}ATT {}{}\
        DEF {}{}DEF {}{}\
        HERO: #ACTION{}MOB: #ACTION{}\
        =============================={}\
        #ACTIONS{}\
        {}{}\
        {}{}\
        {}{}\
        {}{}\
        Defated mobs {}
        ",
        termion::cursor::Hide,
        termion::cursor::Goto(1, 1),
        termion::cursor::Goto(30, 1),
        termion::cursor::Goto(1, 3),
        hero_entity.hp,
        termion::cursor::Goto(30, 3),
        mob_entity.hp,
        termion::cursor::Goto(1, 4),
        hero_entity.damage,
        termion::cursor::Goto(30, 4),
        mob_entity.damage,
        termion::cursor::Goto(1, 5),
        hero_entity.defence,
        termion::cursor::Goto(30, 5),
        mob_entity.defence,
        termion::cursor::Goto(1, 8),
        termion::cursor::Goto(30, 8),
        termion::cursor::Goto(1, 10),
        termion::cursor::Goto(1, 12),
        termion::cursor::Goto(1, 13),
        {
            let attack_str = String::from("Attack");
            if select_action == EntityActions::Attack {
                attack_str.add(&String::from(" ").add(&KEY_SELECT.to_string()))
            } else {
                attack_str
            }
        },
        termion::cursor::Goto(1, 14),
        {
            let defence_str = String::from("Defence");
            if select_action == EntityActions::Defence {
                defence_str.add(&String::from(" ").add(&KEY_SELECT.to_string()))
            } else {
                defence_str
            }
        },
        termion::cursor::Goto(1, 15),
        {
            let nothing_str = String::from("Nothing");
            if select_action == EntityActions::Nothing {
                nothing_str.add(&String::from(" ").add(&KEY_SELECT.to_string()))
            } else {
                nothing_str
            }
        },
        termion::cursor::Goto(1, 16),
        {
            let flee_str = String::from("Flee");
            if select_action == EntityActions::Flee {
                flee_str.add(&String::from(" ").add(&KEY_SELECT.to_string()))
            } else {
                flee_str
            }
        },
        termion::cursor::Goto(1, 17),
        defeated_mobs,
    )
    .unwrap();
    stdout.flush().unwrap();
}
