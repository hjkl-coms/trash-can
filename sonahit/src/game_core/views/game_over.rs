use crate::types::input::StdOutTermion;
use std::io::Write;

pub fn view(stdout: &mut StdOutTermion) {
    write!(
        stdout,
        "{}{}{}GameOver{}Press enter to proceed to main menu",
        termion::clear::All,
        termion::cursor::Goto(1, 1),
        termion::cursor::Goto(21, 2),
        termion::cursor::Goto(7, 3)
    )
    .unwrap();
}
