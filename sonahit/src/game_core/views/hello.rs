use crate::types::input::StdOutTermion;
use std::io::Write;

pub fn view(stdout: &mut StdOutTermion) {
    write!(
        stdout,
        "{}{}{}RPG{}PRESS ENTER TO START OR Q TO EXIT",
        termion::clear::All,
        termion::cursor::Goto(1, 1),
        termion::cursor::Goto(21, 2),
        termion::cursor::Goto(6, 4),
    )
    .unwrap();
}
