use crate::types::game_actions::GameMenuActions;
use crate::types::input::StdOutTermion;
use crate::types::keys;

use std::io::Write;

pub fn view(stdout: &mut StdOutTermion, action: GameMenuActions) -> () {
    write!(
        stdout,
        "{}{}{}PRESS key down or up to navigate{}START{}{}EXIT{}",
        termion::clear::All,
        termion::cursor::Hide,
        termion::cursor::Goto(1, 1),
        termion::cursor::Goto(1, 2),
        if action == GameMenuActions::Start {
            keys::KEY_SELECT
        } else {
            keys::KEY_EMPTY
        },
        termion::cursor::Goto(1, 3),
        if action == GameMenuActions::Exit {
            keys::KEY_SELECT
        } else {
            keys::KEY_EMPTY
        },
    )
    .unwrap();
    stdout.flush().unwrap();
}
