use crate::types::entity::{Entity, Hero};

impl Hero {
    pub fn new(hp: i32, damage: i32, defence: i32) -> Self {
        Hero {
            entity: Entity::new(hp, damage, defence),
        }
    }
}
