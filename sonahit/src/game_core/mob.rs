use crate::types::entity::{Entity, Mob};
use crate::types::entity_actions::{Damage, Defence, Health};
use rand::Rng;

impl Mob {
    pub fn new(hp: i32, damage: i32, defence: i32) -> Self {
        Mob {
            entity: Entity::new(hp, damage, defence),
        }
    }
}

impl Damage for Mob {
    fn attack(&mut self, defender: &mut Entity) -> () {
        defender.drop_health(self.get_attack() - defender.get_defence());
    }

    fn get_attack(&self) -> i32 {
        self.entity.damage * (rand::thread_rng().gen_range(1..5))
    }
}
