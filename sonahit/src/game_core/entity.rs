use crate::types::entity::Entity;
use crate::types::entity_actions::{Action, Damage, Defence, EntityActions, Health};

#[allow(dead_code)]
impl Entity {
    pub fn new(hp: i32, damage: i32, defence: i32) -> Self {
        Entity {
            hp: hp,
            damage: damage,
            defence: defence,
        }
    }
}

impl Health for Entity {
    fn add_health(&mut self, points: i32) -> i32 {
        self.hp = match self.hp + points {
            add if add <= 0 => 0,
            add => add,
        };
        self.hp
    }

    fn drop_health(&mut self, points: i32) -> i32 {
        self.hp = match self.hp - points {
            drop if drop <= 0 => 0,
            drop => drop,
        };
        self.hp
    }

    fn is_dead(&self) -> bool {
        self.hp <= 0
    }
}

impl Damage for Entity {
    fn attack(&mut self, other: &mut Entity) -> () {
        other.drop_health(self.get_attack() - other.get_defence());
    }

    fn get_attack(&self) -> i32 {
        self.damage
    }
}

impl Defence for Entity {
    fn defence(&mut self, other: &Entity) -> () {
        self.drop_health(other.get_attack() - self.get_defence());
    }

    fn get_defence(&self) -> i32 {
        self.defence
    }
}

impl Action for Entity {
    fn action(&mut self, other: &mut Entity, action: EntityActions) {
        match action {
            EntityActions::Attack => self.attack(other),
            EntityActions::Defence => self.defence(other),
            _ => (),
        }
    }
}
