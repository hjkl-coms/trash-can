extern crate rand;
extern crate termion;

pub mod game_core;
pub mod types;

use crate::game_core::game::init_game;

fn main() {
    init_game();
}
