extern crate tokio;
mod future;

use future::*;
use tokio::task as tokio_task;

async fn read_file(filename: &str) {
    let file_read = FileReadFuture::new(String::from(filename));

    let file_contents = file_read.await;
    match file_contents {
        Ok(buffer) => println!("{}", buffer),
        Err(err) => println!("{} {} because {}", "Could not read file", filename, err),
    }
}

#[tokio::main]
async fn main() {
    let (executor, spawner) = new_executor_and_started();

    spawner.spawn(read_file("./Cargo.toml"));
    let handler = tokio_task::spawn(read_file("./Cargo.toml"));
    handler.await;

    drop(spawner);
    executor.run();
}
