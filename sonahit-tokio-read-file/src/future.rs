use {
    futures::{
        future::{BoxFuture, FutureExt},
        task::{waker_ref, ArcWake},
    },
    std::{
        fs,
        fs::File,
        future::Future,
        io::{BufRead, BufReader},
        path::Path,
        pin::Pin,
        sync::mpsc::{sync_channel, Receiver, SyncSender},
        sync::{Arc, Mutex},
        task::{Context, Poll, Waker},
    },
};

pub struct FileReadState {
    buffer: Result<String, String>,
    completed: bool,
    waker: Option<Waker>,
}

pub struct FileReadFuture {
    state: Arc<Mutex<FileReadState>>,
}

impl Future for FileReadFuture {
    type Output = Result<String, String>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut state = self.state.lock().unwrap();

        if state.completed {
            Poll::Ready(state.buffer.clone())
        } else {
            state.waker = Some(cx.waker().clone());
            Poll::Pending
        }
    }
}

impl FileReadFuture {
    pub fn new(filepath: String) -> Self {
        let state = Arc::new(Mutex::new(FileReadState {
            buffer: Err(String::new()),
            completed: false,
            waker: None,
        }));

        let thread_state = state.clone();

        std::thread::spawn(move || {
            let mut state = thread_state.lock().unwrap();
            let path = fs::canonicalize(Path::new(&filepath));
            if let Ok(real_path) = path {
                let file = File::open(real_path);
                if let Ok(f) = file {
                    let mut reader = BufReader::new(f);
                    let mut int_buffer = String::new();
                    loop {
                        let bytes_read = reader.read_line(&mut int_buffer);
                        match bytes_read {
                            Ok(b) => {
                                if b <= 0 {
                                    break;
                                }
                            }
                            Err(_) => break,
                        }
                    }
                    state.buffer = Ok(int_buffer.clone());
                } else {
                    state.buffer = Err(format!("{}", file.unwrap_err()));
                }
            } else if let Err(e) = path {
                state.buffer = Err(format!("{}", e));
            }
            state.completed = true;
            if let Some(waker) = state.waker.take() {
                waker.wake()
            }
        });

        FileReadFuture { state }
    }
}

pub struct Task {
    future: Mutex<Option<BoxFuture<'static, ()>>>,
    task_sender: SyncSender<Arc<Task>>,
}

pub struct Executor {
    ready_queue: Receiver<Arc<Task>>,
}

#[derive(Clone)]
pub struct Spawner {
    task_sender: SyncSender<Arc<Task>>,
}

impl Spawner {
    pub fn spawn(&self, future: impl Future<Output = ()> + 'static + Send) {
        let future = future.boxed();
        let task = Arc::new(Task {
            future: Mutex::new(Some(future)),
            task_sender: self.task_sender.clone(),
        });

        self.task_sender.send(task).expect("Too many queues");
    }
}

impl ArcWake for Task {
    fn wake_by_ref(arc_self: &Arc<Self>) {
        let cloned = arc_self.clone();

        arc_self.task_sender.send(cloned).expect("Too many queues");
    }
}

impl Executor {
    pub fn run(&self) {
        while let Ok(task) = self.ready_queue.recv() {
            let mut future_slot = task.future.lock().unwrap();

            if let Some(mut future) = future_slot.take() {
                let waker = waker_ref(&task);
                let ctx = &mut Context::from_waker(&*waker);

                if let Poll::Pending = future.as_mut().poll(ctx) {
                    *future_slot = Some(future);
                }
            }
        }
    }
}

pub fn new_executor_and_started() -> (Executor, Spawner) {
    const MAX_QUEUE: usize = 1000;
    let (task_sender, ready_queue) = sync_channel(MAX_QUEUE);

    (Executor { ready_queue }, Spawner { task_sender })
}
