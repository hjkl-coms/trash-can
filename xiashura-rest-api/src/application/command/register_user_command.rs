pub struct RegisterUserCommand {
    nickname: String,
    first_name: String,
    last_name: String,
    email: String,
}

impl RegisterUserCommand {
    pub fn new(
        nickname: String,
        first_name: String,
        last_name: String,
        email: String,
    ) -> RegisterUserCommand {
        RegisterUserCommand {
            nickname: String,
            first_name: String,
            last_name: String,
            email: String,
        }
    }

    pub fn nickname(&self) -> &String {
        &self.nickname
    }

    pub fn first_name(&self) -> &String {
        &self.first_name
    }

    pub fn last_name(&self) -> &String {
        &self.last_name
    }

    pub fn email(&self) -> &String {
        &self.email
    }
}
