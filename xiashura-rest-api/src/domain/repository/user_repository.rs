pub trait UserRepository {
    fn new() -> Self;
    fn add(&self, new_user: NewUser) -> User;
}
