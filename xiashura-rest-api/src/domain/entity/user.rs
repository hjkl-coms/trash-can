use std::time::SystemTime;
use uuid::Uuid;

pub struct user {
    id: Uuid,
    nickname: Option<String>,
    first_name: Option<String>,
    last_name: Option<String>,
    email: Option<String>,
    created_at: SystemTime,
}

impl User {
    pub fn new(
        id: Uuid,
        nickname: Option<String>,
        first_name: Option<String>,
        last_name: Option<String>,
        email: Option<String>,
        created_at: SystemTime,
    ) -> Self {
        Self {
            id: id,
            nickname: nickname,
            first_name: first_name,
            last_name: last_name,
            email: email,
            created_at: created_at,
        }
    }

    fn apply(&self, event: Event) {
        match event {
            Event::RegisteredUser => {
                let mut data = RegisteredUserEvent {
                    nickname: self.nickname.clone().unwrap(),
                    first_name: self.first_name.clone().unwrap(),
                    last_name: self.last_name.clone().unwrap(),
                    email: self.email.clone().unwrap(),
                };
                post_event!(&EVENT_BUS, &mut data, RegisteredUserEvent);
            }
        }
    }

    pub fn register_user(&self) -> () {
        self.apply(Event::RegisteredUser);
    }
}
