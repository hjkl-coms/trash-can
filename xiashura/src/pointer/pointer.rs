#[warn(dead_code)]
pub struct S<'a, 'b> {
    id: &'a i32,
    val: &'b i32,
}

#[cfg(test)]
mod tests {
    use super::S;

    #[test]
    fn test_pointer() {
        let x = 10;
        let r;
        {
            let y = 20;
            {
                let s = S { id: &x, val: &y };
                r = s.id;
                assert!(r == &10);
            }
        }
        //линка истекает и все беда :-(
        assert!(r == &10);
    }
}
