pub struct Simple {
    name: String,
}

impl Simple {
    pub fn new(name: String) -> Self {
        Simple { name: name }
    }

    pub fn println_simple_struct(&self) {
        print!("name {}", &self.name);
    }
}
