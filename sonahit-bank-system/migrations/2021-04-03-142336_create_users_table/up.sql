-- Your SQL goes here
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    login CHARACTER VARYING NOT NULL,
    name CHARACTER VARYING,
    password CHARACTER VARYING NOT NULL,
    UNIQUE (login)
);