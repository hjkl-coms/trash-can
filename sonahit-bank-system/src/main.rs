#![feature(proc_macro_hygiene, decl_macro)]
#![allow(clippy::single_match)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate rocket_contrib;
extern crate serde;
extern crate serde_json;

mod connection;
mod http;
mod middlewares;
mod models;
mod routes;
mod schema;
mod types;

use dotenv::dotenv;

fn main() {
    dotenv().ok();

    routes::build()
        .register(catchers![
            http::error_handler::bad_request,
            http::error_handler::not_found,
            http::error_handler::unprocessable_entity,
            http::error_handler::internal_error
        ])
        .launch();
}
