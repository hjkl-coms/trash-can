use crate::schema::users;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Queryable, Insertable, Identifiable)]
#[table_name = "users"]
pub struct User {
    pub id: Option<i32>,
    pub login: String,
    pub name: Option<String>,
    #[serde(skip)]
    pub password: String,
}

impl User {
    pub fn new(login: String, password: String, name: Option<String>) -> Self {
        User {
            id: None,
            login,
            name,
            password,
        }
    }
}
