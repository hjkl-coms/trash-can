table! {
    users (id) {
        id -> Int4,
        login -> Varchar,
        name -> Nullable<Varchar>,
        password -> Varchar,
    }
}
