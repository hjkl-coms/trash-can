use diesel::pg::PgConnection;
use diesel::prelude::*;
use std::env;

pub struct DbConnection<'a> {
    pub connection: &'a PgConnection,
}

impl<'a> DbConnection<'a> {
    pub fn create() -> PgConnection {
        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        PgConnection::establish(&database_url)
            .expect(&format!("Error connecting to {}", database_url))
    }
}
