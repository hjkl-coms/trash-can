use crate::http::controllers;
use rocket::Rocket;

pub fn build() -> Rocket {
    rocket::ignite()
        .mount("/", routes![controllers::home::index,])
        .mount("/auth", routes![controllers::auth::signup,])
}
