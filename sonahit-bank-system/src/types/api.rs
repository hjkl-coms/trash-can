use rocket::http::{ContentType, Status};
use rocket::request::Request;
use rocket::response::{Responder, Response, Result};
use rocket_contrib::json;
use serde::Serialize;
use std::collections::HashMap;

#[derive(Clone)]
pub struct ApiResponse<T> {
    pub complete: bool,
    pub status: Status,
    pub message: Option<String>,
    pub errors: Option<HashMap<String, String>>,
    pub data: Option<T>,
}

impl<T> ApiResponse<T> {
    pub fn incomplete(
        status: Status,
        message: Option<String>,
        errors: Option<HashMap<String, String>>,
        data: Option<T>,
    ) -> Self {
        ApiResponse::new(false, status, message, data, errors)
    }

    pub fn complete(
        status: Status,
        message: Option<String>,
        data: Option<T>,
        errors: Option<HashMap<String, String>>,
    ) -> Self {
        ApiResponse::new(true, status, message, data, errors)
    }

    pub fn new(
        complete: bool,
        status: Status,
        message: Option<String>,
        data: Option<T>,
        errors: Option<HashMap<String, String>>,
    ) -> Self {
        ApiResponse {
            complete,
            status,
            message,
            errors,
            data,
        }
    }
}

impl<'r, T> Responder<'r> for ApiResponse<T>
where
    T: Serialize,
{
    fn respond_to(self, request: &Request) -> Result<'r> {
        let message = self
            .message
            .unwrap_or_else(|| "Success".to_string())
            .as_str()
            .to_owned();
        let mut json =
            json!({"message": message, "status": self.complete, "code": self.status.code });
        if let Some(data) = self.data {
            json = json!({"message": message, "status": self.complete, "data": data, "code": self.status.code});
        } else if let Some(errors) = self.errors {
            json = json!({"message": message, "status": self.complete, "errors": errors, "code": self.status.code});
        }

        Response::build_from(json.respond_to(&request).unwrap())
            .status(self.status)
            .header(ContentType::JSON)
            .ok()
    }
}
