use rocket::FromForm;
use serde::Deserialize;

#[derive(Deserialize, FromForm, Debug)]
pub struct Register {
    pub login: String,
    pub name: Option<String>,
    pub password: String,
}
