use crate::types::api::ApiResponse;
use rocket::http::Status;
use rocket_contrib::json::JsonValue;

#[catch(422)]
pub fn unprocessable_entity() -> ApiResponse<JsonValue> {
    ApiResponse {
        complete: false,
        data: None,
        message: Some(String::from("Unprocessable entity")),
        errors: None,
        status: Status::UnprocessableEntity,
    }
}

#[catch(400)]
pub fn bad_request() -> ApiResponse<JsonValue> {
    ApiResponse {
        complete: false,
        data: None,
        message: Some(String::from("Bad Request")),
        errors: None,
        status: Status::BadRequest,
    }
}

#[catch(500)]
pub fn internal_error() -> ApiResponse<JsonValue> {
    ApiResponse {
        complete: false,
        data: None,
        message: Some(String::from("Internal error exception")),
        errors: None,
        status: Status::BadRequest,
    }
}

#[catch(404)]
pub fn not_found() -> ApiResponse<JsonValue> {
    ApiResponse {
        complete: false,
        data: None,
        message: Some(String::from("Route not found")),
        errors: None,
        status: Status::NotFound,
    }
}
