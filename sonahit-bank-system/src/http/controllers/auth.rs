use crate::connection::DbConnection;
use crate::http::forms::register::Register;
use crate::models::user::User;
use crate::schema::users;
use crate::schema::users::dsl::*;
use crate::types::api::ApiResponse;
use bcrypt::hash;
use diesel::RunQueryDsl;
use rocket::http::Status;
use rocket_contrib::json::Json;
use rocket_contrib::json::JsonError;

#[post("/signup", format = "application/json", data = "<dto>")]
pub fn signup(dto: Result<Json<Register>, JsonError>) -> ApiResponse<User> {
    let status = Status::Ok;
    let msg = "Ok".to_string();
    match dto {
        Err(err) => {
            let msg = match err {
                JsonError::Io(msg) => msg.to_string(),
                JsonError::Parse(_, msg) => msg.to_string(),
            };
            let status = Status::UnprocessableEntity;
            ApiResponse::incomplete(status, Some(msg), None, None)
        }
        Ok(dto) => {
            let db_conn = DbConnection::create();
            let user_password = hash(dto.password.clone(), 10).unwrap();
            let mut user = User::new(dto.login.clone(), user_password, dto.name.clone());
            let result = diesel::insert_into(users)
                .values(&user)
                .returning(users::id)
                .get_result::<i32>(&db_conn);
            drop(db_conn);
            match result {
                Ok(user_id) => {
                    user.id = Some(user_id);
                    ApiResponse::complete(status, Some(msg), Some(user), None)
                }
                Err(err) => {
                    let msg = format!("{}", err);
                    ApiResponse::incomplete(status, Some(msg), None, None)
                }
            }
        }
    }
}
