use std::cmp::Ordering;

pub mod my_calculator {
    pub fn Sum<T>(a1: T, a2: T) -> T {
        println!(
            "{} + {} = {}",
            a1: u32 = a1.trim().parse().expect("Error"),
            a2: u32 = a2.trim().parse().expect("Error"),
            a1 + a2,
        );
    }

    // Сравнение
    pub fn my_ordering() -> Ordering {
        a1: u32 = a1.trim().parse().expect("Error");
        a2: u32 = a2.trim().parse().expect("Error");

        if a1 < a2 => println!("Hello");

        match a1.cmp(&a2) {
            Ordering::Less => return Ordering::Less,
            Ordering::Greater => return Ordering::Greater,
            Ordering::Equal => return Ordering::Equal,
        };
    }
}
