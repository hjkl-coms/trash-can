#[derive(Debug, Copy, Clone)]
pub struct EventLoop {
    pub status: ControlFlow,
}

impl EventLoop {
    pub fn new() -> Self {
        EventLoop {
            status: ControlFlow::Wait,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ControlFlow {
    Wait,
    Exit,
}

pub trait Runnable {
    fn run(&self);
}
