pub mod lang {
    use serde::Deserialize;
    use serde::Serialize;
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Alphabet {
        pub length: u8,
        pub characters: Vec<String>,
    }

    #[derive(Serialize, Deserialize, Debug)]
    pub struct Dictionary {
        pub words: Vec<String>,
    }
}

pub mod text {
    use serde::Deserialize;
    use serde::Serialize;

    #[derive(Serialize, Deserialize, Debug)]
    pub struct Text {
        pub topic: String,
        pub content: Vec<String>,
    }

    #[derive(Serialize, Deserialize, Debug)]
    pub struct Texts {
        pub texts: Vec<Text>,
    }
}

pub mod terminal {
    #[allow(dead_code)]
    pub const BACKSPACE_SEQ: u32 = 8u32;

    pub trait Ncurses {
        fn setup(&self);
        fn tear_down(&self);
    }
}
