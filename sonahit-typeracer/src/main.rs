extern crate colored;
extern crate rand;
extern crate serde;
extern crate tokio;

mod event_loop;
mod type_racer;
mod types;

#[tokio::main]
async fn main() {
    tokio::task::spawn(type_racer::run_keyboard_event_loop());
}
