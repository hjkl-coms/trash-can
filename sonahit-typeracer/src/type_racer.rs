use super::types;
use ncurses as nc;

use super::event_loop;
use super::event_loop::*;
use event_loop::Runnable;
use rand::seq::SliceRandom;
use rand::thread_rng;
use std::fs::File;
use std::io::Read;
use types::lang::{Alphabet, Dictionary};
use types::terminal::Ncurses;

use serde_json as json;

#[allow(dead_code)]
fn get_characters() -> Option<Vec<String>> {
    let cwd = std::env::current_dir().unwrap();

    let path = cwd.join("assets/").join("alphabet.json");

    let file = File::open(path);

    if let Ok(mut file_content) = file {
        let mut contents = String::new();

        file_content.read_to_string(&mut contents).unwrap();

        let Alphabet { characters, .. } = json::from_str(contents.as_str()).unwrap();

        Some(characters)
    } else {
        None
    }
}

fn get_words() -> Option<Vec<String>> {
    let cwd = std::env::current_dir().unwrap();

    let path = cwd.join("assets/").join("dictionary.json");

    let file = File::open(path);

    if let Ok(mut file_content) = file {
        let mut contents = String::new();

        file_content.read_to_string(&mut contents).unwrap();

        let Dictionary { words, .. } = json::from_str(contents.as_str()).unwrap();

        Some(words)
    } else {
        None
    }
}

const OK_PAIR: i16 = 1;
const ERROR_PAIR: i16 = 2;
const WRITING_PAIR: i16 = 3;
const PENDING_PAIR: i16 = 4;
const BG_COLOR: i16 = nc::COLOR_BLACK;
const OK_COLOR: i16 = nc::COLOR_GREEN;
const ERROR_COLOR: i16 = nc::COLOR_RED;
const WRITING_COLOR: i16 = nc::COLOR_WHITE;
const PENDING_COLOR: i16 = 8; // GRAY

fn print_key(ch: char, pair: i16) {
    nc::attr_on(nc::COLOR_PAIR(pair));

    nc::addstr(format!("{}", ch).as_str());

    nc::attr_off(nc::COLOR_PAIR(pair));
}

pub async fn run_keyboard_event_loop() {
    let event_loop = event_loop::EventLoop::new();

    event_loop.setup();
    event_loop.run();
    event_loop.tear_down();
}

fn move_right(w: nc::WINDOW) {
    if nc::getcurx(w) <= nc::getmaxx(w) {
        let mut y = nc::getcury(w);

        if y >= 0 {
            for x in nc::getcurx(w)..=nc::getmaxx(w) {
                nc::mv(y, x);

                if (nc::inch() & nc::A_CHARTEXT()) != ' ' as u32 {
                    break;
                }
            }
        }

        if nc::getcurx(w) == nc::getmaxx(w) && y < nc::getmaxy(w) {
            y += 1;
            nc::mv(y, nc::getcurx(w));
        }
    } else {
        let y = nc::getcury(w);
        let x = nc::getcurx(w);
        nc::mv(y, x + 1);
    }
}

impl Ncurses for EventLoop {
    fn setup(&self) {
        let w = nc::initscr();

        // raw mode
        nc::raw();

        // Enable scrolling
        nc::scrollok(w, true);

        // Enable keypad expanded
        nc::keypad(w, true);

        nc::start_color();

        nc::init_pair(OK_PAIR, OK_COLOR, BG_COLOR);

        nc::init_pair(ERROR_PAIR, ERROR_COLOR, BG_COLOR);

        nc::init_pair(WRITING_PAIR, WRITING_COLOR, BG_COLOR);

        nc::init_pair(PENDING_PAIR, PENDING_COLOR, BG_COLOR);

        // noecho
        nc::noecho();

        // enable ctrl+c
        nc::cbreak();
    }

    fn tear_down(&self) {
        nc::nocbreak();

        nc::endwin();
    }
}

impl Runnable for EventLoop {
    fn run(&self) {
        let w = nc::stdscr();
        let characters = get_words().unwrap();
        let iterator = characters.iter();
        let mut round = 0;
        let take_number = 10;
        let mut vec_str = iterator.clone().collect::<Vec<&String>>();
        vec_str.shuffle(&mut thread_rng());
        let mut it_str = vec_str.iter().clone();
        let mut is_err = false;
        let mut err_x = 0;
        let mut err_y = 0;
        let mut char_counter: usize = 0;

        nc::addstr(
            it_str
                .clone()
                .take(take_number)
                .map(|v| String::from(*v))
                .collect::<Vec<String>>()
                .join(" ")
                .as_str(),
        );

        let mut curr_string = it_str.next().unwrap();
        nc::mv(0, 0);

        loop {
            let res = nc::wget_wch(w);
            if let Some(nc::WchResult::Char(ch)) = res {
                let char_pressed = std::char::from_u32(ch);

                let curr_char = curr_string.chars().nth(char_counter);
                match char_pressed {
                    Some(char_pressed) if char_pressed == '\n' => {
                        nc::wmove(w, 0, 0);
                        nc::wclrtoeol(w);
                        round += 1;
                        is_err = false;
                        err_x = 0;
                        err_y = 0;
                        char_counter = 0;
                        it_str.nth((take_number * round) - 1);
                        nc::addstr(
                            it_str
                                .clone()
                                .take(take_number)
                                .map(|v| String::from(*v))
                                .collect::<Vec<String>>()
                                .join(" ")
                                .as_str(),
                        );
                        curr_string = it_str.next().unwrap();
                        nc::mv(0, 0);
                    }
                    Some(char_pressed) => {
                        if let Some(curr_char) = curr_char {
                            let curr_pair = if char_pressed == curr_char {
                                char_counter += 1;
                                if is_err {
                                    nc::mv(err_y, err_x);
                                }
                                is_err = false;
                                OK_PAIR
                            } else {
                                if !is_err {
                                    err_x = nc::getcurx(w);
                                    err_y = nc::getcury(w);
                                    is_err = true;
                                }
                                ERROR_PAIR
                            };
                            if !is_err {
                                print_key(curr_char, curr_pair);
                            }
                            if curr_pair == ERROR_PAIR {
                                nc::mv(err_y, err_x);
                                print_key(curr_char, curr_pair);
                                nc::mv(err_y, err_x + 1);
                            }
                        } else {
                            char_counter = 0;
                            if let Some(curr) = it_str.next() {
                                curr_string = curr;
                                move_right(w);
                            }
                        }
                    }
                    None => (),
                }
            };

            if self.status == ControlFlow::Exit {
                nc::clear();

                break;
            };

            nc::refresh();
        }
    }
}
